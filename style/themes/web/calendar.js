// It shows the date
hidefromoldbrowsers
function GetDay(intDay) {
  var DayArray = new Array('воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота')
  return DayArray[intDay]
}
function GetMonth(intMonth) {
  var MonthArray = new Array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря')
  return MonthArray[intMonth]
}
function getDateStr() {
  var today = new Date()
  var year = today.getYear()
  if (year < 1000) year += 1900
  var todayStr = GetDay(today.getDay()) + ', '
  todayStr += today.getDate() + ' ' + GetMonth(today.getMonth())
  todayStr += ', ' + year
  return todayStr
}